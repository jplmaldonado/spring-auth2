package com.backend.demo.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.backend.demo.models.User;
import com.backend.demo.repository.UserRepository;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent>{

	@Autowired
	UserRepository userRepository;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		List<User> users = userRepository.findAll(); 
		
		if(users.isEmpty()) {
			this.createUsers("João Maldonado", "jmaldonado.ti@gmail.com", "123456");
			this.createUsers("Teste2", "teste2.ti@gmail.com", "123456");
			this.createUsers("Teste3", "teste3.ti@gmail.com", "123456");
		} 
	}
	
	public void createUsers(String name, String email, String password) {
		User user = new User();
		user.setEmail(email);
		user.setName(name);
		user.setPassword(password);
		
		userRepository.save(user);
	}
}
