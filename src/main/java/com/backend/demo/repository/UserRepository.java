package com.backend.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.backend.demo.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
}
